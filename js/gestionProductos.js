var productosObtenidos ;
function getProductos(){
  var url = "https://services.odata.org/V4/Northwind/Northwind.svc/Products";

  var request = new XMLHttpRequest();
  request.onreadystatechange = function(){
    if(this.readyState == 4 || this.status == 200){
      //console.table(JSON.parse(request.responseText).value);
      productosObtenidos = request.responseText;
      procesarProductos();
    }
  }
  request.open("GET", url, true);
  request.send();
}

function procesarProductos(){
  var JSONProductos = JSON.parse(productosObtenidos);
  var divTabla = document.getElementById("divTabla");
  //if(divTabla.childNodes.length >= 1){
  divTabla.removeChild(divTabla.firstChild);
  //}
  var tabla = document.createElement("table");
  var tbody = document.createElement("tbody");
  var thead = document.createElement("thead");

  tabla.classList.add("table");
  tabla.classList.add("table-hover");
  //alert(JSONProductos.value[0].ProductName);
  for (var i = 0; i < JSONProductos.value.length ; i++){
    //console.log(JSONProductos.value[i].ProductName);
    var nuevaFila = document.createElement("tr");

    var columnaNombre = document.createElement("td");
    columnaNombre.innerText = JSONProductos.value[i].ProductName;

    var columnaPrecio = document.createElement("td");
    columnaPrecio.innerText = JSONProductos.value[i].UnitPrice;

    var columnaStock = document.createElement("td");
    columnaStock.innerText = JSONProductos.value[i].UnitsInStock;

    nuevaFila.appendChild(columnaNombre);
    nuevaFila.appendChild(columnaPrecio);
    nuevaFila.appendChild(columnaStock);

    tbody.appendChild(nuevaFila);

  }

  var cabecera = document.createElement("tr");
  var productName = document.createElement("td");
  productName.innerText = "Nombre Producto";

  var unitPrice = document.createElement("td");
  unitPrice.innerText = "Precio Unitario";

  var unitsInStock = document.createElement("td");
  unitsInStock.innerText = "Unidades en Stock";

  cabecera.appendChild(productName);
  cabecera.appendChild(unitPrice);
  cabecera.appendChild(unitsInStock);
  thead.appendChild(cabecera);

  tabla.appendChild(thead);
  tabla.appendChild(tbody);
  divTabla.appendChild(tabla);
}
