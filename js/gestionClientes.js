var clientes;
function getCustomers(){
  //var url = "https://services.odata.org/V4/Northwind/Northwind.svc/Customers?$filter=Country eq 'Germany'";
  var url = "https://services.odata.org/V4/Northwind/Northwind.svc/Customers";

  var request = new XMLHttpRequest();
  request.onreadystatechange = function(){
    if(this.readyState == 4 || this.status == 200){
      //console.table(JSON.parse(request.responseText).value);
      clientes = request.responseText;
      procesarClientes();
    }
  }
  request.open("GET", url, true);
  request.send();
}

function procesarClientes(){
  var rutaBandera = "https://www.countries-ofthe-world.com/flags-normal/flag-of-";
  var JSONClientes = JSON.parse(clientes);
  var divTabla = document.getElementById("divTabla");
  //if(divTabla.childNodes.length >= 1){
  divTabla.removeChild(divTabla.firstChild);
  //}
  var tabla = document.createElement("table");
  var tbody = document.createElement("tbody");
  var thead = document.createElement("thead");

  tabla.classList.add("table");
  tabla.classList.add("table-hover");

  for(var i = 0; i < JSONClientes.value.length; i ++){
    var nuevaFila =  document.createElement("tr");
    var columnaNombre = document.createElement("td");
    columnaNombre.innerText = JSONClientes.value[i].ContactName;

    var columnaCiudad = document.createElement("td");
    columnaCiudad.innerText = JSONClientes.value[i].City;

    var columnaPais = document.createElement("td");
    var pais = JSONClientes.value[i].Country;

    var imgBandera = document.createElement("img");
    imgBandera.classList.add("flag");

    if(pais == "UK")
      imgBandera.src = rutaBandera + "United-Kingdom.png";
    else
      imgBandera.src = rutaBandera + pais + ".png";

    columnaPais.appendChild(imgBandera);

    nuevaFila.appendChild(columnaNombre);
    nuevaFila.appendChild(columnaCiudad);
    nuevaFila.appendChild(columnaPais);

    tbody.appendChild(nuevaFila);

  }
  var cabecera = document.createElement("tr");
  var name = document.createElement("td");
  name.innerText = "Nombre Cliente";

  var city = document.createElement("td");
  city.innerText = "Ciudad";

  var country = document.createElement("td");
  country.innerText = "País";

  cabecera.appendChild(name);
  cabecera.appendChild(city);
  cabecera.appendChild(country);
  thead.appendChild(cabecera);

  tabla.appendChild(thead);
  tabla.appendChild(tbody);
  divTabla.appendChild(tabla);

}
